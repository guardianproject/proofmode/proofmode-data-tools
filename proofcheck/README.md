# Proofcheck consistency and synchrony checks

- Install dependencies, found in requirements.txt

- Run on the command line:

  - Run the following command:

    `python main.py`

  - When prompted, add the batch JSON file name, including extension. (Make sure that all files are in the same folder!)

OR

- Open the notebook and follow the instructions

# ProofZip_to_Viz.py

- Open terminal from directory which has the ProofMode zip file

  - Run on the command line:
    
    `python ProofZip_to_Viz.py`
  
  -When prompted, type in the exact name of the ProofMode zip file (including ".zip")