import numpy as np
import pandas as pd
import altair as alt
import json
import re

name_of_download_proofmode_json = input('Type in the exact name of the resulting JSON file for ProofMode data: ')
name_of_download_exif_json = input('Type in the exact name of the resulting JSON file for exif data: ')


def renaming_function_file(name_of_download_proofmode_json):
    proofmode_df = pd.read_json(name_of_download_proofmode_json)
    new_column_names = []
    for (columnName, columnData) in proofmode_df.iteritems():
        columnName_split = columnName.replace(".", " ")
        for ind, char in enumerate(columnName[:-1]):
            if columnName[ind].islower() and columnName[ind+1].isupper():
                columnName_split = columnName[:ind +
                                              1] + ' ' + columnName[ind+1:]
        new_column_names.append(columnName_split)

    proofmode_df.columns = new_column_names
    return proofmode_df

def change_df_index(proofmode_df):
    proofmode_photo_df = proofmode_df.copy(deep=True)
    proofmode_photo_df['File Name'] = ''
    for index, row in proofmode_photo_df.iterrows():
        file_name = row["File Path"].split("/")[-1]
        proofmode_photo_df.at[index, 'File Name'] = file_name
    proofmode_photo_df = proofmode_photo_df.set_index('File Name')
    return proofmode_photo_df

def use_file_name_as_outer_key(proofmode_photo_df):
    proofmode_photo_dict = proofmode_photo_df.to_dict('index')
    return(proofmode_photo_dict)

def change_dict_to_pandas_df(proofmode_photo_dict):
    nested_proofmode_photo_df = pd.DataFrame.from_dict(proofmode_photo_dict, orient = 'index')
    # print(nested_proofmode_photo_df)
    return nested_proofmode_photo_df

def creating_exif_df(name_of_download_exif_json):
    exif_df = pd.read_json(name_of_download_exif_json)
    exif_df.replace("", np.nan, inplace=True)
    exif_df = exif_df.T
    # print(exif_df)
    return exif_df

def comparing_exif_and_proofmode_metadata(exif_df, nested_proofmode_photo_df):
    comparison_df = pd.DataFrame(index=nested_proofmode_photo_df.index)
    for (proofmode_filename, proofmode_row) in nested_proofmode_photo_df.iterrows():
        for (exif_filename, exif_row) in exif_df.iterrows():
            if proofmode_filename == exif_filename:
                if 'Make' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF Make'] = exif_df.at[exif_filename,'Make']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF Make'] = ""
                if 'Manufacturer' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Manufacturer'] = nested_proofmode_photo_df.at[proofmode_filename,'Manufacturer']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Manufacturer'] = ""
                if 'Model' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF Model'] = exif_df.at[exif_filename,'Model']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF Model'] = ""
                if 'Hardware' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Hardware'] = nested_proofmode_photo_df.at[proofmode_filename,'Hardware']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Hardware'] = ""
                if 'DateTime' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF DateTime'] = exif_df.at[exif_filename,'DateTime']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF DateTime'] = ""
                if 'File Created' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Created'] = nested_proofmode_photo_df.at[proofmode_filename,'File Created']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Created'] = ""
                if 'File Modified' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Modified'] = nested_proofmode_photo_df.at[proofmode_filename,'File Modified']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Modified'] = ""
                if 'Proof Generated' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Proof Generated'] = nested_proofmode_photo_df.at[proofmode_filename,'Proof Generated']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Proof Generated'] = ""
                if 'Location Time' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Time'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Time']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Time'] = ""
                if 'Location Latitude' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Latitude'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Latitude']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Latitude'] = ""
                if 'Location Longitude' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Longitude'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Longitude']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Location Longitude'] = ""


    # print(exif_df.columns)
    # print(nested_proofmode_photo_df.columns)
    # print(comparison_df)
    return comparison_df

proofmode_df = renaming_function_file(name_of_download_proofmode_json)
proofmode_photo_df = change_df_index(proofmode_df)
proofmode_photo_dict = use_file_name_as_outer_key(proofmode_photo_df)
nested_proofmode_photo_df = change_dict_to_pandas_df(proofmode_photo_dict)
exif_df = creating_exif_df(name_of_download_exif_json)
comparison = comparing_exif_and_proofmode_metadata(exif_df, nested_proofmode_photo_df)