
import zipfile
import os
from datetime import date
import pandas as pd
import numpy as np
import glob
import json
from dateutil.parser import parse
import geopandas
import geopy.distance
import folium
from geographiclib.geodesic import Geodesic
from PIL import Image, ExifTags, TiffImagePlugin
from pillow_heif import register_heif_opener
import ntpath
ntpath.basename("a/b/c")
register_heif_opener()


zip_file_path = input('Type in the exact name of the ProofMode zipfile (includeing .zip): ')
current_working_directory = os.getcwd()
extract_path = current_working_directory + "/proofpack_unzipped"
todays_date = str(date.today())
JSON = '.json'

with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
    zip_ref.extractall(extract_path)

def batch_JSON(extract_path):
    newfilename = "ProofMode_JSON_data_" + todays_date + JSON
    batch = list()

    for proof_file in glob.glob(extract_path + "/**/*" + JSON, recursive=True):
        with open(proof_file, 'r') as input_file:
            batch.append(json.load(input_file))

    with open(newfilename, 'w') as output_file:
        json.dump(batch, output_file, indent=4, sort_keys=True)
        # json.dump(batch, output_file, indent=4)
        output_file.close()

    ProofMode_JSON_batch = newfilename

    return ProofMode_JSON_batch

def exif_extraction(extract_path):
    newfilename = "ProofMode_EXIF_data_" + todays_date + JSON
    exif_data_dict = {}
    # list_of_image_file_types = [".jpg", ".JPG", ".Jpg", ".jpeg", ".JPEG", ".Jpeg", ".HEIC", ".heic", ".Heic"]

    for image_file in glob.glob(extract_path+ "/**/*", recursive = True):
        
        if (image_file.endswith(".jpg")) or (image_file.endswith(".jpeg")) or (image_file.endswith(".JPG")) or (image_file.endswith(".HEIC")):
            image_open = Image.open(image_file)
            image_open.verify()
            image_getexif = image_open.getexif()
            if image_getexif:
                head, tail = ntpath.split(image_file)
                for (k, v) in image_getexif.items():
                    exif = {
                        ExifTags.TAGS[k]: v for k, v in image_getexif.items() if k in ExifTags.TAGS and type(v) not in [bytes, TiffImagePlugin.IFDRational]
                    }
                    exif_data_dict[tail] = exif

    exif_keys = list(exif_data_dict.keys())
    exif_keys.sort()
    sorted_exif_data_dict = {i: exif_data_dict[i] for i in exif_keys}

    with open(newfilename, 'w') as output_file:
        json_exif_data = json.dump(sorted_exif_data_dict, output_file, indent=4, sort_keys=True)
        # json_exif_data = json.dump(sorted_exif_data_dict, output_file, indent=4)
        output_file.close()

    ProofMode_EXIF_batch = newfilename

    return ProofMode_EXIF_batch

def renaming_function_file(ProofMode_JSON_batch):
    proofmode_df = pd.read_json(ProofMode_JSON_batch)
    new_column_names = []
    for (columnName, columnData) in proofmode_df.iteritems():
        columnName_split = columnName.replace(".", " ")
        for ind, char in enumerate(columnName[:-1]):
            if columnName[ind].islower() and columnName[ind+1].isupper():
                columnName_split = columnName[:ind + 1] + ' ' + columnName[ind+1:]
        new_column_names.append(columnName_split)

    proofmode_df.columns = new_column_names
    return proofmode_df

def change_df_index(proofmode_df):
    proofmode_photo_df = proofmode_df.copy(deep=True)
    proofmode_photo_df['File Name'] = ''
    for index, row in proofmode_photo_df.iterrows():
        file_name = row["File Path"].split("/")[-1]
        proofmode_photo_df.at[index, 'File Name'] = file_name
    proofmode_photo_df = proofmode_photo_df.set_index('File Name')
    return proofmode_photo_df

def use_file_name_as_outer_key(proofmode_photo_df):
    proofmode_photo_dict = proofmode_photo_df.to_dict('index')
    return(proofmode_photo_dict)

def change_dict_to_pandas_df(proofmode_photo_dict):
    nested_proofmode_photo_df = pd.DataFrame.from_dict(proofmode_photo_dict, orient = 'index')
    return nested_proofmode_photo_df

def nested_ProofMode_JSON_batch(nested_proofmode_photo_df):
    nested_json_df = nested_proofmode_photo_df.to_json(orient="index")
    parsed_nested_json_df = json.loads(nested_json_df)
    nested_ProofMode_JSON_batch_file = "nested_ProofMode_JSON_batch_" + todays_date + JSON
    with open(nested_ProofMode_JSON_batch_file, 'w') as output_file:
        nested_ProofMode_JSON_batch = json.dump(parsed_nested_json_df, output_file, indent=4, sort_keys=True)
        output_file.close()
    return nested_ProofMode_JSON_batch_file

def creating_exif_df(ProofMode_EXIF_batch):
    exif_df = pd.read_json(ProofMode_EXIF_batch)
    exif_df = exif_df.replace(np.nan, "")
    exif_df = exif_df.T
    return exif_df

def comparing_exif_and_proofmode_metadata(exif_df, nested_proofmode_photo_df):
    comparison_df = pd.DataFrame(index=nested_proofmode_photo_df.index)
    for (proofmode_filename, proofmode_row) in nested_proofmode_photo_df.iterrows():
        for (exif_filename, exif_row) in exif_df.iterrows():
            if proofmode_filename == exif_filename:
                if 'Make' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF Make'] = exif_df.at[exif_filename,'Make']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF Make'] = ""
                if 'Manufacturer' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Manufacturer'] = nested_proofmode_photo_df.at[proofmode_filename,'Manufacturer']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Manufacturer'] = ""
                if 'Model' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF Model'] = exif_df.at[exif_filename,'Model']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF Model'] = ""
                if 'Hardware' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Hardware'] = nested_proofmode_photo_df.at[proofmode_filename,'Hardware']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Hardware'] = ""
                if 'DateTime' in exif_df:
                    comparison_df.at[proofmode_filename, 'EXIF DateTime'] = exif_df.at[exif_filename,'DateTime']
                else:
                    comparison_df.at[proofmode_filename, 'EXIF DateTime'] = ""
                if 'File Created' in nested_proofmode_photo_df:
                    # print('Original: ', nested_proofmode_photo_df.loc[proofmode_filename, "File Created"])
                    # raw = str(nested_proofmode_photo_df.loc[proofmode_filename, "File Created"])
                    # nested_proofmode_photo_df[proofmode_filename,'File Created'] = parse(raw, fuzzy=True)
                    # print("String parsed: ", nested_proofmode_photo_df[proofmode_filename,'File Created'])
                    # nested_proofmode_photo_df[proofmode_filename, 'File Created'] = pd.to_datetime(nested_proofmode_photo_df[proofmode_filename, 'File Created'], format='%Y:%m:%d %H:%M:%S-%Z')
                    # print("Convert to date time: ", nested_proofmode_photo_df[proofmode_filename,'File Created'])
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Created'] = nested_proofmode_photo_df.at[proofmode_filename,'File Created']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Created'] = ""
                if 'File Modified' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Modified'] = nested_proofmode_photo_df.at[proofmode_filename,'File Modified']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE File Modified'] = ""
                if 'Proof Generated' in nested_proofmode_photo_df:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Proof Generated'] = nested_proofmode_photo_df.at[proofmode_filename,'Proof Generated']
                else:
                    comparison_df.at[proofmode_filename, 'PROOFMODE Proof Generated'] = ""
                # if 'Location Time' in nested_proofmode_photo_df:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Time'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Time']
                # else:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Time'] = ""
                # if 'Location Latitude' in nested_proofmode_photo_df:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Latitude'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Latitude']
                # else:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Latitude'] = ""
                # if 'Location Longitude' in nested_proofmode_photo_df:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Longitude'] = nested_proofmode_photo_df.at[proofmode_filename,'Location Longitude']
                # else:
                #     comparison_df.at[proofmode_filename, 'PROOFMODE Location Longitude'] = ""

    comparison_df = comparison_df.replace(np.nan, "")
    comparison_df = comparison_df.sort_index()
    comparison_df = comparison_df.style.set_properties(**{'border': '1px black solid !important'}, **{'background-color': 'lightblue'}, subset=['EXIF Make','EXIF Model', 'EXIF DateTime'])
    comparison_df = comparison_df.set_properties(
    **{'border': '1px black solid !important'},
    **{'background-color': 'lightgreen'}, subset=['PROOFMODE Manufacturer','PROOFMODE Hardware', 'PROOFMODE File Created', 'PROOFMODE File Modified', 'PROOFMODE Proof Generated']  
).set_table_styles([{
    'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')],
    'props': [('border', '2px black solid !important')]}]
).set_caption("EXIF/ProofMode Metadata Highlights").set_table_styles([{
    'selector': 'caption',
    'props': [
        ('color', 'black'),
        ('font-size', '25px'),
        ('text-align','center'),
        ('border', '3px black solid !important')
    ]
}], overwrite=False)
    
    comparison_df = comparison_df.set_table_styles([{'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')]}], overwrite=False)

    return comparison_df


def synchrony_time_elapsed(proofmode_df):
    list_of_times = []

    for index, row in proofmode_df.iterrows():
        raw = proofmode_df.loc[index, "File Created"]
        try:
            file_timestamp = parse(raw, fuzzy=True)
            list_of_times.append(file_timestamp)
        except Exception as e:
            print(e)

    time_initial = min(list_of_times)
    time_final = max(list_of_times)
    time_elapsed = time_final - time_initial
    return time_elapsed

def create_geo_df(proofmode_df):
    geo_df = proofmode_df[["Device ID", "Location Provider", "Location Latitude", "Location Longitude"]]
    geo_df["Location Longitude"] = pd.to_numeric(geo_df["Location Longitude"])
    geo_df["Location Latitude"] = pd.to_numeric(geo_df["Location Latitude"])
    geo_df = geo_df.dropna()
    geo_df = geo_df.drop_duplicates()
    return geo_df


def synchrony_difference_between_points_in_km(geo_df):
    if not geo_df.empty:
        print(geo_df)
        differences_km = []
        coords_set_a = []
        coords_set_b = []
        for i, row in geo_df.iterrows():
            latitude_i = geo_df.loc[i]['Location Latitude']
            longitude_i = geo_df.loc[i]['Location Longitude']
            for j, row in geo_df.iterrows():
                latitude_j = geo_df.loc[j]['Location Latitude']
                longitude_j = geo_df.loc[j]['Location Longitude']
                if latitude_i != latitude_j and longitude_i != longitude_j:
                    coords_i = (latitude_i, longitude_i)
                    coords_j = (latitude_j, longitude_j)
                    diff_between_one_set_of_points_km = geopy.distance.geodesic(coords_i, coords_j).km
                    differences_km.append(diff_between_one_set_of_points_km)
                    coords_set_a.append(coords_i)
                    coords_set_b.append(coords_j)
        list_of_coordinates_and_differences = list(zip(coords_set_a, coords_set_b, differences_km))
        differences_km_df = pd.DataFrame(list_of_coordinates_and_differences, columns = ['Coordinates Set A', 'Coordinates Set B', 'Differences in km'])
        max_radius = differences_km_df['Differences in km'].max()
        if len(geo_df) > 1:
            coords_max_difference_df = differences_km_df[differences_km_df['Differences in km'] == differences_km_df['Differences in km'].max()]
            coords_max_a = coords_max_difference_df['Coordinates Set A'].loc[coords_max_difference_df.index[0]]
            coords_max_lat_a, coords_max_lon_a = coords_max_a
            coords_max_b = coords_max_difference_df['Coordinates Set B'].loc[coords_max_difference_df.index[0]]
            coords_max_lat_b, coords_max_lon_b = coords_max_b

        else:
            coords_max_difference_df = differences_km_df[differences_km_df['Differences in km'] == differences_km_df['Differences in km'].max()]
            coords_max_lat_a = geo_df.loc[0]['Location Latitude']
            coords_max_lon_a = geo_df.loc[0]['Location Longitude']
            coords_max_lat_b = geo_df.loc[0]['Location Latitude']
            coords_max_lon_b = geo_df.loc[0]['Location Longitude']

        return differences_km_df, max_radius, coords_max_difference_df, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b

    else:
        pass



def synchrony_centroid_map(geo_df, max_radius, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b):
    geojson_df = geopandas.GeoDataFrame(geo_df, geometry=geopandas.points_from_xy(
        geo_df["Location Latitude"], geo_df['Location Longitude']))
    x_list = geojson_df['Location Latitude']
    y_list = geojson_df['Location Longitude']
    # centroid_of_coordinates = geojson_df.dissolve().centroid
    # x_centroid = centroid_of_coordinates.centroid.x
    # y_centroid = centroid_of_coordinates.centroid.y

    if len(geojson_df) > 1:
        path_from_a_to_b = Geodesic.WGS84.Inverse(coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b)

        midpoint_array = Geodesic.WGS84.Direct(coords_max_lat_a, coords_max_lon_a, path_from_a_to_b['azi1'], path_from_a_to_b['s12']/2)
        
        midpoint_lat = midpoint_array['lat2']
        midpoint_lon = midpoint_array['lon2']

        centroid_map = folium.Map(location=[midpoint_lat, midpoint_lon])

        folium.Circle([midpoint_lat, midpoint_lon],
                        radius=max_radius/2*1000
                       ).add_to(centroid_map)

        all_coordinates = zip(x_list, y_list)
        for i in all_coordinates:
            folium.Marker(i, popup="Proof Point", icon=folium.Icon(
                color='green')).add_to(centroid_map)

        sw = geo_df[['Location Latitude', 'Location Longitude']].min().values.tolist()
        ne = geo_df[['Location Latitude', 'Location Longitude']].max().values.tolist()
        centroid_map.fit_bounds([sw, ne]) 
    else:
        centroid_map = folium.Map(location=[coords_max_lat_a, coords_max_lon_a])

        folium.Circle([coords_max_lat_a, coords_max_lon_a]
                       ).add_to(centroid_map)
        folium.Marker([coords_max_lat_a, coords_max_lon_a], popup="Proof Point", icon=folium.Icon(
                color='green')).add_to(centroid_map)
        sw = geo_df[['Location Latitude', 'Location Longitude']].min().values.tolist()
        ne = geo_df[['Location Latitude', 'Location Longitude']].max().values.tolist()
        centroid_map.fit_bounds([sw, ne]) 

    return geojson_df, centroid_map

def check_synchrony(proofmode_df):
    time_elapsed = synchrony_time_elapsed(proofmode_df)
    total_time_elapsed = time_elapsed.total_seconds()
    geo_df = create_geo_df(proofmode_df)

    if not geo_df.empty:
        differences_km_df, max_radius, coords_max_difference_df, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b = synchrony_difference_between_points_in_km(geo_df)
        geojson_df, centroid_map = synchrony_centroid_map(geo_df, max_radius, coords_max_lat_a, coords_max_lon_a, coords_max_lat_b, coords_max_lon_b)
        if not os.path.exists('outputs'):
            os.makedirs('outputs')
        centroid_map.save(outfile='outputs/centroid_map.html')
        with open("outputs/centroid_map.html", "r", encoding='utf8') as html_file:
            centroid_map_html = ""
            for readline in html_file:
                line_strip = readline.strip()
                centroid_map_html += line_strip
            json.dumps({
            "time_elapsed": time_elapsed.total_seconds(),
            "geojson": json.loads(geojson_df.to_json()),
            "differences_km_df": json.loads(differences_km_df.to_json()),
            "centroid_map_html": centroid_map_html})
        total_area_covered = 3.14*max_radius**2
        synchrony_df = pd.DataFrame([[total_time_elapsed,total_area_covered]],columns=['Total Time Elapsed (seconds)','Area Covered (km)'])
        synchrony_df = synchrony_df.rename(index={0: 'ProofPack Summary'})
        synchrony_df = synchrony_df.style.set_properties(
            **{'border': '1px black solid !important'},
            ).set_table_styles([{
                'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')],
                'props': [('border', '2px black solid !important')]}]
            ).set_caption("ProofPack Summary Stats").set_table_styles([{
                'selector': 'caption',
                'props': [
                    ('color', 'black'),
                    ('font-size', '25px'),
                    ('text-align','center'),
                    ('border', '3px black solid !important')
                ]
            }], overwrite=False)
    
        synchrony_df = synchrony_df.set_table_styles([{'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')]}], overwrite=False)

        return synchrony_df, centroid_map_html

    else:
        centroid_map_html = ""
        total_area_covered = "Unknown"
        synchrony_df = pd.DataFrame([[total_time_elapsed,total_area_covered]],columns=['Total Time Elapsed (seconds)','Area Covered (km)'])
        synchrony_df  = synchrony_df.rename(index={0: 'ProofPack Summary'})
        synchrony_df = synchrony_df.style.set_properties(
            **{'border': '1px black solid !important'},
            ).set_table_styles([{
                'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')],
                'props': [('border', '2px black solid !important')]}]
            ).set_caption("ProofPack Summary Stats").set_table_styles([{
                'selector': 'caption',
                'props': [
                    ('color', 'black'),
                    ('font-size', '25px'),
                    ('text-align','center'),
                    ('border', '3px black solid !important')
                ]
            }], overwrite=False)
        return synchrony_df, centroid_map_html
        
def html_dataframes(comparison_df, synchrony_df):
    html_table = comparison_df.to_html()
    legend_dict = {'EXIF Metadata': ["Blue"], 'ProofMode Metadata': ["Green"]}
    legend_df = pd.DataFrame.from_dict(legend_dict)
    legend_df  = legend_df.rename(index={0: 'Color Code'})
    legend_df = legend_df.style.set_properties(**{'border': '1px black solid !important'}, **{'background-color': 'lightblue'}, subset=['EXIF Metadata'], overwrite=False)
    legend_df = legend_df.set_properties(
        **{'border': '1px black solid !important'},
        **{'background-color': 'lightgreen'}, subset=['ProofMode Metadata']
        ).set_table_styles([{
                'selector': 'th', 'props': [('font-size', '10pt'),('border-style','solid'),('border-width','1px')],
                'props': [('border', '2px black solid !important')]}]
            ).set_caption("Legend").set_table_styles([{
                'selector': 'caption',
                'props': [
                    ('color', 'black'),
                    ('font-size', '15px'),
                    ('text-align','center'),
                    ('border', '3px black solid !important')
                ]
            }], overwrite=False)
    legend_table = legend_df.to_html()
    synchrony_table = synchrony_df.to_html()
    return html_table, synchrony_table, legend_table

def generate_html(extract_path, legend_table, html_table, synchrony_table, centroid_map_html, comparison_df, exif_df, nested_proofmode_photo_df):
    html_content = """
                    <!DOCTYPE html>
                            <html>
                            <head>
                                <title>ProofPack Unzipped</title>
                            </head>
                            <body>
                                <h1>Welcome to ProofPack Unzipped</h1>
                    """

    html_content += synchrony_table + "<br><br>"

    html_content += legend_table + "<br><br>"

    html_content += html_table + "<br><br>"

    html_content += centroid_map_html

    for filename in os.listdir(extract_path):
        if filename.endswith(".jpg") or filename.endswith(".jpeg") or filename.endswith(".JPG") or filename.endswith(".HEIC"):  # Assuming these formats
            img_path = os.path.join(extract_path, filename)
            html_content += f'<img src="{img_path}" width="400" alt="{filename}"><br>'
            # for index, row in comparison_df.iterrows():
            #     if index == filename:
            #         dataframe_row = row.to_frame()
            #         html_row = dataframe_row.to_html()
            #         html_content += "<figcaption>" + index + "</figcaption><br>"
            #         html_content += html_row + "<br><br>"
            html_content += "<figcaption>" + filename + "</figcaption><br>"
            html_content += "<h3>EXIF Data</h3>"
            for index, row in exif_df.iterrows():
                if index == filename:
                    dataframe_row = row.to_frame()
                    html_row = dataframe_row.to_html()
                    html_content += html_row + "<br>"
            html_content += "<h3> ProofMode Data</h3>"
            for index, row in nested_proofmode_photo_df.iterrows():
                if index == filename:
                    dataframe_row = row.to_frame()
                    html_row = dataframe_row.to_html()
                    # html_content += "<figcaption>" + index + "</figcaption><br>"
                    html_content += html_row + "<br><br>"


    html_content += "</body></html>"

    with open("proofpack_unzipped.html", "w") as file:
        file.write(html_content)
    return html_content

ProofMode_JSON_batch = batch_JSON(extract_path)
ProofMode_EXIF_batch = exif_extraction(extract_path)
proofmode_df = renaming_function_file(ProofMode_JSON_batch)
proofmode_photo_df = change_df_index(proofmode_df)
proofmode_photo_dict = use_file_name_as_outer_key(proofmode_photo_df)
nested_proofmode_photo_df = change_dict_to_pandas_df(proofmode_photo_dict)
nested_ProofMode_JSON_batch_file = nested_ProofMode_JSON_batch(nested_proofmode_photo_df)
exif_df = creating_exif_df(ProofMode_EXIF_batch)
comparison_df = comparing_exif_and_proofmode_metadata(exif_df, nested_proofmode_photo_df)
synchrony_df, centroid_map_html = check_synchrony(proofmode_df)
html_table, synchrony_table, legend_table = html_dataframes(comparison_df, synchrony_df)
html_content = generate_html(extract_path, legend_table, html_table, synchrony_table, centroid_map_html, comparison_df, exif_df, nested_proofmode_photo_df)