import fs from "fs"
import path from "path"
import process from "process"
import geojson from "geojson"
import tokml from "tokml"

var proofDir = process.argv[2]
var exportType = process.argv[3]
const proofPoints = [];
var lastPoint;

function getProof (targetDir, isLast) {

fs.readdirSync(targetDir).forEach(file => {
    // Make one pass and make the file complete
    var fromPath = path.join(targetDir, file);

      if (fromPath.endsWith(".proof.json"))
      {

	var parentDir = (path.dirname(fromPath))
        var proofZip = parentDir + ".zip"
        var proofCid = ""
  
        console.log("found proof: " + proofZip)

        if (fs.existsSync(proofZip + ".cid"))
        { 
           let proofCiddata = fs.readFileSync(proofZip + ".cid")
           proofCid = proofCiddata
        } 

        console.log("parsing proof: " + fromPath);
	let rawdata = fs.readFileSync(fromPath);
	let proof = JSON.parse(rawdata);
 
        var thisPoint = {name: proof["File Modified"], hash: proof["File Hash SHA256"], device: proof["Hardware"], proofCheck: "https://proofcheck.gpfs.link/#" + proofCid, description: "<a href='https://proofcheck.gpfs.link/#" + proofCid + "'>Verify this on ProofCheck</a><br/><a href='https://w3s.link/ipfs/" + proofCid + "'>Download proof+media on Web3.storage</a>", latitude: parseFloat(proof["Location.Latitude"]), longitude: parseFloat(proof["Location.Longitude"])}
        if (thisPoint.latitude && thisPoint.longitude) {

        console.log("adding point: " + thisPoint);
        proofPoints.push(thisPoint);

        if (lastPoint && lastPoint.longitude && lastPoint.latitude )
        	proofPoints.push({line: [[lastPoint.longitude, lastPoint.latitude], [thisPoint.longitude, thisPoint.latitude]]})

        lastPoint = thisPoint;
        }
      }
      else if (fs.lstatSync(fromPath).isDirectory())
      {
        getProof(fromPath)
      }	
 

  });
}

function generateKml (points) {

  const geojsonObject = geojson.parse(points, { Point: ['latitude', 'longitude'], LineString: 'line' })
  if (exportType == "geojson")
  { 
  	console.log(JSON.stringify(geojsonObject));
  }
  else
  {
  	const response = tokml(geojsonObject);
  	console.log((response));
  }
}

function main () {

  if (proofDir)
  {
     getProof(proofDir, true);
     generateKml(proofPoints);
  } 
  else
  {
    console.log("proofToGeoWeb3 <path-to-proof-directory-base> <export-type:geojson or kml>")
  }
}

main();
